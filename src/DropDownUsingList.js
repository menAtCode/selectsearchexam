import React from "react";
import classes from "./DropDownUsingList.module.scss";

function DropDownUsingList(props) {
  const { datas, menu, setMenu, selectedItem, setSelectedItem, noCode } = props;

  const [openDropDown, setOpenDropDown] = React.useState(false);

  const [indexValue, setIndexValue] = React.useState(-1);

  let [length] = [menu.length];

  let liRef = React.useRef({});

  React.useLayoutEffect(() => {
    if (!openDropDown) setIndexValue(-1);
  }, [openDropDown]);

  const HandleChange = (e) => {
    const searchValue = e.target?.value;

    const newArray = datas.filter((value) => {
      if (searchValue === "") {
        return value;
      } else if (
        value?.name.toLowerCase().includes(searchValue?.toLowerCase())
      ) {
        return value;
      }
    });

    if (newArray?.length === 0) {
      setMenu([
        {
          name: "No Result Found"
        }
      ]);
    } else {
      setMenu(newArray);
    }
  };

  const HandleOnclickList = (e, data) => {
    if (noCode) setSelectedItem(data?.name);
    else setSelectedItem(data?.name + " " + data.code);
    setOpenDropDown((prevState) => !prevState);
  };

  const HandleEnter = (e) => {
    switch (e.key) {
      case "ArrowDown":
        moveDown(e);

        break;

      case "ArrowUp":
        moveUp(e);

        break;

      case "Enter":
        EnterData();
        break;

      default:
        break;
    }
  };

  const moveDown = (e) => {
    setIndexValue((prevState) => (prevState + 1) % length);
  };

  const moveUp = (e) => {
    if (indexValue === 0) setIndexValue(0);
    setIndexValue((prevState) => (length + (prevState - 1)) % length);
  };

  const EnterData = () => {
    const menuResult = menu[indexValue];
    if (noCode) setSelectedItem(menuResult?.name);
    else setSelectedItem(menuResult?.name + " " + menuResult.code);
    setOpenDropDown((prevState) => !prevState);
  };

  React.useLayoutEffect(() => {
    liRef[indexValue + 5]?.scrollIntoView({
      behavior: "smooth",
      block: "end",
      inline: "nearest"
    });
  }, [indexValue]);

  return (
    <div className={classes["container"]}>
      <button
        className={`${classes["Button"]} ${
          openDropDown && classes["button-border"]
        }`}
        onClick={() => setOpenDropDown((prevState) => !prevState)}
      >
        <span className={classes["title"]}>
          {selectedItem ? selectedItem : "-SELECT-"}
        </span>
        <span className={classes["arrow-down"]}>&#8744;</span>
      </button>
      <ul
        className={
          openDropDown
            ? classes["unodered-list"]
            : classes["unodered-list-none"]
        }
      >
        <li className={classes["unodered-list-input"]}>
          <input
            onKeyDown={(e) => HandleEnter(e)}
            className={classes["input"]}
            placeholder="Type Here..."
            onChange={HandleChange}
            autoComplete="off"
          />
        </li>
        {React.useMemo(() => {
          return menu?.map((items, index) => {
            return (
              <li
                ref={(el) => (liRef[index] = el)}
                className={`${classes["list-style"]} ${
                  indexValue === index && classes["list-active"]
                }`}
                key={index}
                onDoubleClick={(e) => HandleOnclickList(e, items)}
              >
                {noCode ? items?.name : `${items?.name}  (${items?.code})`}
              </li>
            );
          });
        }, [menu, indexValue])}
      </ul>
    </div>
  );
}

export default DropDownUsingList;
