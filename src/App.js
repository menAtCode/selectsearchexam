import Countries from "./listOfCountries";
import ProgrammingLaguanges from "./listOfLanguage";
import classes from "./App.module.scss";
import DropDownUsingList from "./DropDownUsingList";
import React from "react";

function App() {
  const [countries, setCountries] = React.useState(Countries);
  const [languagMenu, setLanguageMenu] = React.useState(ProgrammingLaguanges);
  const [selectedItem, setSelectedItem] = React.useState("");
  const [selectedLanguage, setSelectedLanguage] = React.useState("");

  return (
    <div className={classes["App"]}>
      <div className={classes["container"]} id="drop-down-wrapper">
        <div className={classes.selectedItems}>
          <h1>Country:{`     ${selectedItem}`}</h1>
          <h1>Language: {`     ${selectedLanguage}`}</h1>
        </div>
        <div className={classes["drop-down-wrapper"]}>
          <div className={classes.country}>
            <label htmlFor="country">Choose a Country:</label>
            <DropDownUsingList
              datas={Countries}
              menu={countries}
              setMenu={setCountries}
              selectedItem={selectedItem}
              setSelectedItem={setSelectedItem}
            />
          </div>
          <div className={classes.country}>
            <label htmlFor="language">Choose a Language:</label>
            <DropDownUsingList
              datas={ProgrammingLaguanges}
              menu={languagMenu}
              setMenu={setLanguageMenu}
              selectedItem={selectedLanguage}
              setSelectedItem={setSelectedLanguage}
              noCode
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
